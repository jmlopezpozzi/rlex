RLEX
RLE File codec

rlex is a very simple RLE based encoding scheme. rlex works by encoding only
spans of similar bytes starting from a threshold value.

A control byte determines what the next bytes mean:

If the highest order bit in the control byte is 0 the next bytes form a span
of different bytes. The length of the span is encoded in the seven lower
order bits of the control byte (with a value of 0 meaning that 1 byte follows).
After that many bytes, another control byte comes next (unless the end of file
is reached).

If the highest order bit in the control byte is 1 the next byte is the value
of an RLE-encoded span. The length of the span is encoded in the seven lower
order bits of the control byte, with 0 meaning the threshold value (which is
3 bytes). After the byte next to the control byte, another control byte is
found (unless the end of file is reached).

rlex is provided as a library which contains the necessary functions to encode
and decode files, write and read the header, etc. Two simple programs that
include the library to perform encoding and decoding are also provided.

rlex_en: encoder program
    usage: rlex_en input_file output_file [options]
	options are: -nh: do not encode the header
                 -pt: performance testing mode

rlex_de: decoder program
    usage: rlex_de input_file output_file [options]
    options are: -nh: do not try to read the header
                 -pt: prints performance info

Both programs are built via make using the gcc compiler. rlex is coded in C99.

Contents of folders:
 bin: Folder where make stores the module binaries before linking.
 span_analyze: Small utility tool that analyzes a file and finds how many
    spans of equal bytes it has.
 src: Source code for the rlex library, encoder and decoder programs and the
    span_analyze utility.
 test: Testing utilities.

Improvements planned:
 Make the decoder read files in batches of bytes instead of byte by byte.
 Option to configure size of said batches.
 Encoder option to run the span_analyze program before encoding and suggest if
  it is worth encoding the file or not.

Exercise by Juanmi Lopez.
