#include "rlex.h"
#include <stdio.h>
#include <string.h>
#include <stdbool.h>

#define MAX_DIFF_INDEX 127
#define MIN_FOR_EQU 3  // Minimum number of similar bytes to perform RLE (always 3)

enum op_modes {DIFF = 0, EQU = 1, END_ENCODE = 2};

/* Private functions */
static enum op_modes get_next_mode(FILE *fp_in);
static enum op_modes mode_diff(FILE *fp_in, FILE *fp_out);
static enum op_modes mode_equ(FILE *fp_in, FILE *fp_out);
static void write_mode_diff(size_t read_bytes, unsigned char *span,
                            FILE *fp_out);
static void write_mode_equ(short read_bytes, int byte_value, FILE *fp_out);

/* rlex header */
static char rlex_header[] = {'r', 'l', 'e', 'x', 0x00, 0x03};


int rlex_write_header(FILE *fp_out)
{
	fwrite(rlex_header, sizeof(char), sizeof(rlex_header) / sizeof(char),
	       fp_out);

	return 0;
}

bool rlex_read_header(FILE *fp_in)
{
	char header_compare[sizeof(rlex_header)];  // Since the type is char, sizeof yields the correct number of elements

	fread(header_compare, sizeof(header_compare[0]), sizeof(header_compare),
	      fp_in);
	if (memcmp(rlex_header, header_compare, sizeof(rlex_header)) == 0) {
		return true;
	}
	return false;
}

/* rlex decoder */
int rlex_decode(FILE *fp_in, FILE *fp_out)
{
	int read_byte;
	size_t control_byte;
	enum op_modes mode;
	unsigned char read_span[MAX_DIFF_INDEX + MIN_FOR_EQU];

	for(;;) {
		read_byte = getc(fp_in);
		if (read_byte == EOF) {
			break;
		}

		mode = read_byte >> 7;
		control_byte = read_byte & 0x7F;

		if (mode == DIFF) {
			fread(read_span, sizeof(*read_span), control_byte + 1, fp_in);
			fwrite(read_span, sizeof(*read_span), control_byte + 1, fp_out);
		}
		else {
			read_byte = getc(fp_in);
			memset(read_span, read_byte, control_byte + MIN_FOR_EQU);
			fwrite(read_span, sizeof(*read_span), control_byte + MIN_FOR_EQU,
			       fp_out);
		}
	}

	return 0;
}

/* rlex encoder */
int rlex_encode(FILE *fp_in, FILE *fp_out)
{
	enum op_modes mode = get_next_mode(fp_in);

	for (;;) {
		switch (mode) {
		case DIFF:
			mode = mode_diff(fp_in, fp_out);
			break;

		case EQU:
			mode = mode_equ(fp_in, fp_out);
			break;

		case END_ENCODE:
		default:
			return 0;
		}
	}
}

static enum op_modes get_next_mode(FILE *fp_in)
{
	long original_pos = ftell(fp_in);
	int tested_byte = getc(fp_in);
	enum op_modes result = EQU;

	if (tested_byte == EOF) {
		result = END_ENCODE;
	}
	else {
		int i;
		for (i = 1; i < MIN_FOR_EQU; ++i) {
			int cur_byte = getc(fp_in);
			if (cur_byte != tested_byte) {
				result = DIFF;
				break;
			}
		}
	}

	fseek(fp_in, original_pos, SEEK_SET);
	return result;
}

static enum op_modes mode_diff(FILE *fp_in, FILE *fp_out)
{
	int cur_byte;
	int prev_byte = EOF;
	int equal_bytes;
	size_t read_bytes = 0;
	unsigned char span[MAX_DIFF_INDEX + 1];

	for (;;) {
		cur_byte = getc(fp_in);

		if (cur_byte == EOF) {
			if (read_bytes > 0) {
				write_mode_diff(read_bytes, span, fp_out);
			}

			return END_ENCODE;
		}

		span[read_bytes] = (unsigned char) cur_byte;
		read_bytes += 1;

		if (cur_byte == prev_byte) {
			equal_bytes += 1;

			if (equal_bytes >= MIN_FOR_EQU) {
				fseek(fp_in, (long) -MIN_FOR_EQU, SEEK_CUR);
				read_bytes -= MIN_FOR_EQU;
				write_mode_diff(read_bytes, span, fp_out);

				return EQU;
			}
		}
		else {
			equal_bytes = 1;
		}

		if (read_bytes > MAX_DIFF_INDEX) {
			if (equal_bytes > 1) {
				fseek(fp_in, (long) -equal_bytes, SEEK_CUR);

				if (get_next_mode(fp_in) == EQU) {
					read_bytes -= equal_bytes;
					write_mode_diff(read_bytes, span, fp_out);

					return EQU;
				}

				fseek(fp_in, (long) equal_bytes, SEEK_CUR);
			}

			write_mode_diff(read_bytes, span, fp_out);

			return get_next_mode(fp_in);
		}

		prev_byte = cur_byte;
	}

	return END_ENCODE;
}

static enum op_modes mode_equ(FILE *fp_in, FILE *fp_out)
{
	int cur_byte = getc(fp_in);
	int prev_byte = cur_byte;
	size_t read_bytes = 0;

	for (;;) {
		if (cur_byte == EOF) {
			if (read_bytes > 0) {
				write_mode_equ(read_bytes, prev_byte, fp_out);
			}

			return END_ENCODE;
		}

		if (cur_byte != prev_byte) {
			write_mode_equ(read_bytes, prev_byte, fp_out);
			fseek(fp_in, -1L, SEEK_CUR);

			return get_next_mode(fp_in);
		}

		read_bytes += 1;

		if (read_bytes >= MAX_DIFF_INDEX + MIN_FOR_EQU) {
			write_mode_equ(read_bytes, cur_byte, fp_out);

			return get_next_mode(fp_in);
		}

		prev_byte = cur_byte;
		cur_byte = getc(fp_in);
	}

	return END_ENCODE;
}

/* Write mode diff */
static void write_mode_diff(size_t read_bytes, unsigned char *span,
                            FILE *fp_out)
{
	putc(read_bytes - 1, fp_out);  // Control byte
	fwrite(span, sizeof(*span), read_bytes, fp_out);
}

/* Write mode equ */
static void write_mode_equ(short read_bytes, int byte_value, FILE *fp_out)
{
	putc((read_bytes - MIN_FOR_EQU) | (0x1 << 7), fp_out);  // Control byte
	putc(byte_value, fp_out);                               // Value of span
}
