/* rlex.h */
#ifndef RLEX_H
 #define RLEX_H

 #include <stdio.h>
 #include <stdbool.h>

int rlex_encode(FILE *fp_in, FILE *fp_out);
int rlex_decode(FILE *fp_in, FILE *fp_out);
int rlex_write_header(FILE *fp_out);
bool rlex_read_header(FILE *pf_in);

#endif

