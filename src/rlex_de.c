/* rlex decoding program */

#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <time.h>
#include "lib/rlex.h"

#define PERFTEST_FILE "./perftest/perftest.out"

int main(int argc, char *argv[])
{
	if (argc < 3) {
		fprintf(stderr,"rlex decoder\n"
		               "usage: rlex_de input_file output_file [-nh -pt]\n");
		return -1;
	}

	struct {
		unsigned noheader: 1;
		unsigned perftest: 1;
	} program_mode = {0, 0};

	FILE *fp_in;
	FILE *fp_out;
	FILE *fp_perftest;
	clock_t decoding_time;
	int i;

	fp_in = fopen(argv[1], "rb");
	if (fp_in == NULL) {
		fprintf(stderr, "Could not open file %s\n", argv[1]);
		return -2;
	}

	fp_out = fopen(argv[2], "wb");
	if (fp_out == NULL) {
		fprintf(stderr, "Could not create file %s\n", argv[2]);
		fclose(fp_in);
		return -3;
	}

	i = 3;
	while (argv[i] != NULL) {
		if (strcmp(argv[i], "-nh") == 0) {
			program_mode.noheader = 1;
			printf("noheader\n");
		}
		else
		if (strcmp(argv[i], "-pt") == 0) {
			program_mode.perftest = 1;
			printf("perftest\n");
			fp_perftest = fopen(PERFTEST_FILE, "ab");
			if (fp_perftest == NULL) {
				fprintf(stderr, "Could not create or open perftest file\n");
				program_mode.perftest = 0;
			}
		}

		++i;
	}

	if (program_mode.noheader == 0) {
		if (rlex_read_header(fp_in) == false) {
			fprintf(stderr, "Invalid header for file %s\n", argv[1]);
			fprintf(stderr, "use option -nh to decode headerless files\n");

			return -4;
		}
	}
	if (program_mode.perftest == 1) {
		decoding_time = clock();
	}

	rlex_decode(fp_in, fp_out);

	if (program_mode.perftest == 1) {
		decoding_time = clock() - decoding_time;
		printf("decoding time was %g clocks\n", (double) decoding_time);
		fprintf(fp_perftest, "%g\n", (double) decoding_time);
		fclose(fp_perftest);
	}

	fclose(fp_in);
	fclose(fp_out);

	return 0;
}
