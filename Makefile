builddir = ./
libd = $(builddir)src/lib/
srcd = $(builddir)src/
bind = $(builddir)bin/

both: $(bind)rlex.o $(bind)rlex_en.o $(bind)rlex_de.o
	gcc -o $(builddir)rlex_en $(bind)rlex.o $(bind)rlex_en.o
	gcc -o $(builddir)rlex_de $(bind)rlex.o $(bind)rlex_de.o

rlex_en: $(bind)rlex.o $(bind)rlex_en.o
	gcc -o $(builddir)rlex_en $(bind)rlex.o $(bind)rlex_en.o

rlex_de: $(bind)rlex.o $(bind)rlex_de.o
	gcc -o $(builddir)rlex_de $(bind)rlex.o $(bind)rlex_de.o

$(bind)rlex_en.o: $(srcd)rlex_en.c $(libd)rlex.h
	gcc -c -Wall -Wextra -std=c99 -pedantic -o $(bind)rlex_en.o $(srcd)rlex_en.c

$(bind)rlex_de.o: $(srcd)rlex_de.c $(libd)rlex.h
	gcc -c -Wall -Wextra -std=c99 -pedantic -o $(bind)rlex_de.o $(srcd)rlex_de.c

$(bind)rlex.o: $(libd)rlex.c $(libd)rlex.h
	gcc -c -Wall -Wextra -std=c99 -pedantic  -o $(bind)rlex.o $(libd)rlex.c

span_analyze: $(srcd)/span_analyze/span_analyze.c
	gcc -Wall -Wextra -std=c99 -pedantic -o $(builddir)span_analyze $(srcd)/span_analyze/span_analyze.c

clean:
	rm -v $(bind)*.o
