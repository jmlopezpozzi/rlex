#!/bin/bash

PTST=./perftest            #perftest working folder
ENCP=../rlex_en            #encoder program
SPAN=../span_analyze       #span analyzer program
ENDE=./codecr/             #encoded and decoded files folder
CASE=./casesr/             #test cases folder
CIDX=./case_index_r.txt    #tests index
PTFL=$PTST/perftest.out    #perftest file
SSFL=$PTST/session.info    #session file
CLKA=./programs/clock_average   #program that averages values printed in perftest file

#Note: The clock_average program must have been previously compiled or the script will fail

span_analysis=0  #set to 1 to use the span analyzer for each test case (span analyzer program must have been compiled previously)
enc_options="-nh -pt"

#first argument sets number of runs per test, default is 10
runs=0
if [ $1 ] ; then
	runs=$1
else
	runs=10
fi

lines=1
run=1

echo "$runs runs per test"
echo

rm -v $SSFL  #deletes previous session file
touch $SSFL  #creates new session file so clock average program can open it
touch $PTFL  #this is done so the first rm in the loop does not complain about perftest file not existing

for i in $(ls $CASE)
do
	rm $PTFL

	sed -n "$lines p" <$CIDX
	let "lines++"

	if [ $span_analysis -eq 1 ] ; then
		$SPAN $CASE$i
	fi

	while [ $run -le $runs ]
	do
		let "run++"
		$ENCP $CASE$i $ENDE$i.rlx $enc_options >/dev/null
	done
	let "run = 1"

	$CLKA
	echo
done
rm $PTFL
