#!/bin/bash

HEXD=./hexdumps/ #hex viewer output
CIDX=./case_index.txt  #tests index

line=1

for i in $(ls $HEXD)
do
	sed -n "$line p" <$CIDX
	let "line++"
	echo
	cat $HEXD$i
	echo
	echo
done
