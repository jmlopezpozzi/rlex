#!/bin/bash

SOUT=./stdout/   #program messages
CIDX=./case_index.txt  #tests index

line=1

for i in $(ls $SOUT)
do
	echo -----
	sed -n "$line p" <$CIDX
	echo
	let "line++"
	cat $SOUT$i
	echo
done
