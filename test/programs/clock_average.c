/* Averages clock values obtained by successive rlex perftest runs */

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>

#define PERFTEST_FILE "./perftest/perftest.out"
#define SESSION_FILE "./perftest/session.info"
#define STR_LEN 20

int main(void)
{
	FILE *fp;
	char line_buffer[STR_LEN+1];
	char *line_cursor;
	double value = 0.0;
	int ch;
	int lines = 0;

	fp = fopen(PERFTEST_FILE, "rb");
	if (fp == NULL) {
		fprintf(stderr, "Could not open perftest file\n");
		return -1;
	}

	while (fgets(line_buffer, STR_LEN + 1, fp) != NULL) {
		++lines;
		errno = 0;
		value += strtod(line_buffer, &line_cursor);
		if (*line_cursor != '\n') {
			fprintf(stderr, "Could not read value in line %d\n", lines);
			fclose(fp);
			return -2;
		}
		if (errno != 0) {
			fprintf(stderr, "Error while reading value in line %d: %s\n",
			                lines, strerror(errno));
		}
	}

	value = value / lines;
	printf("Average clocks: %g\n", value);
	fclose(fp);

	fp = fopen(SESSION_FILE, "rb+");
	if (fp == NULL) {
		fprintf(stderr, "Could not open session file\n");
		return -3;
	}

	lines = 1;
	ch = getc(fp);
	while (ch != EOF) {
		if (ch == '\n') {
			++lines;
		}
		ch = getc(fp);
	}
	fprintf(fp, "Test %d: Average clocks %g\n", lines, value);

	fclose(fp);

	return 0;  // May fail
}
