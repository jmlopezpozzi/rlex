#!/bin/bash

ENCP=../rlex_en        #encoder program
DECP=../rlex_de        #decoder program
EQUT=./programs/areequal        #file equality tester
HEXV=./programs/hexview         #hex viewer program
CASE=./cases/          #test cases folder
ENDE=./codec/          #encoded and decoded files folder
HEXD=./hexdumps/       #hex viewer output
SOUT=./stdout/         #program messages
CIDX=./case_index.txt  #tests index

#Note: the file equality tester and the hex viewer programs must have been
#compiled previously, otherwise the script will fail

#if first argument is 'r' uses "real world" cases
if [ -z $1 ]
then
	echo
elif [ $1 = "r" ]
then
	echo
	CASE=./casesr/
	CIDX=./case_index_r.txt
	ENDE=./codecr/
fi

#remove old files from ENDE folder
rm -v $ENDE*

#options are -nh: noheader -pt: perftest
enc_options=""
dec_options=""

vg=""  #set to "valgrind" to use valgrind (-q to only prints errors)

line=1

for i in $(ls $CASE)
do
	sed -n "$line p" <$CIDX
	let "line++"

	$vg $ENCP $CASE$i $ENDE$i.rlx $enc_options >$SOUT$i.stdout
	if [ -z $1 ]
	then
		$HEXV $ENDE$i.rlx >$HEXD$i.hex   #only saves hex dumps for synth cases
	fi
	$vg $DECP $ENDE$i.rlx $ENDE$i.de $dec_options >/dev/null
	$EQUT $CASE$i $ENDE$i.de
	echo
done
